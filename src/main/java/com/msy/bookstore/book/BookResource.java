package com.msy.bookstore.book;

import com.msy.bookstore.entity.Book;
import com.msy.bookstore.mapper.BookMapper;
import com.msy.bookstore.model.BookHal;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameters;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Path("/book")
public class BookResource {
    private static final Logger LOG = Logger.getLogger(BookResource.class.getName());

    @Inject
    private BookService bookService;

    @Inject
    private BookMapper bookMapper;


    @POST
    @Operation(summary = "Create a Book.")
    @APIResponses({
            @APIResponse(responseCode = "200", description = "The created Book object.",
                    content = @Content(mediaType = "application/hal+json",
                            schema = @Schema(type = SchemaType.OBJECT, implementation = BookHal.class))
            )
    })
    @Parameters({
            @Parameter(name = "newBook", description = "Create Book object.", required = true,
                    content = @Content(schema = @Schema(implementation = Book.class)))
    })
    public Response createBook(@Context UriInfo uriInfo, @Valid Book newBook) {
        LOG.log(Level.INFO, "REST request to save Book : {0}.", newBook);
        if (newBook.getId() != null) {
            throw new RuntimeException("A Book need no id for create.");
        }
        if (  newBook.getTransactionTimeZone() == null){
//            LocalDateTime rightNow = LocalDateTime.now();
//            System.out.println("current datetime : " + rightNow);
//            LocalDateTime date = LocalDateTime.now();
//            Date date = Date
//            newBook.setTransactionTimeZone(Date date);
//            LocalDateTime dateTime;
            LocalDateTime str = LocalDateTime.now();
            Timestamp timestamp = Timestamp.valueOf(str);
            newBook.setTransactionTimeZone(timestamp);

        }
        bookService.create(newBook);
        return Response
                .ok(bookMapper.map(newBook, uriInfo))
                .build();
    }



    @GET
    @Operation(summary = "Get a list of all book.")
    @APIResponses({
            @APIResponse(responseCode = "200", description = "The list of all book.",
                    content = @Content(mediaType = "application/hal+json",
                            schema = @Schema(type = SchemaType.ARRAY, implementation = BookHal.class))
            )
    })
    public List<Book> getAllBook(@Context UriInfo uriInfo) {
        LOG.log(Level.INFO, "REST request to get all book.");
        List<Book> allData = bookService.findAll();
        return allData;
    }


    @DELETE
    @Path("/{id}")
    @Operation(summary = "Delete the book with id.")
    @APIResponses({
            @APIResponse(responseCode = "200", description = "book with id is deleted.")
    })
    @Parameters({
            @Parameter(name = "id", description = "The book id. ", required = true)
    })
    public Response removeBook(@PathParam("id") @NotNull Long id) {
        LOG.log(Level.INFO, "REST request to delete book : {0}", id);
        bookService.find(id)
                .ifPresent(book -> bookService.remove(book));
        return Response.ok().build();
    }


    @GET
    @Path("/{id}")
    @Operation(summary = "Get the book with id.")
    @APIResponses({
            @APIResponse(responseCode = "200", description = "The book with id.",
                    content = @Content(mediaType = "application/hal+json",
                            schema = @Schema(type = SchemaType.OBJECT, implementation = BookHal.class))
            ),
            @APIResponse(responseCode = "400", description = "Book not found.")
    })
    @Parameters({
            @Parameter(name = "id", description = "The book id. ", required = true)
    })
    public Response getBook(@Context UriInfo uriInfo, @PathParam("id") @NotNull Long id) {
        LOG.log(Level.INFO, "REST request to get Book : {0}", id);
        return bookService.find(id)
                .map(book -> Response.ok(bookMapper.map(book, uriInfo)).build())
                .orElseGet(() -> Response.status(Response.Status.NOT_FOUND).build());
    }


    @PUT
    @Operation(summary = "Update a book.")
    @APIResponses({
            @APIResponse(responseCode = "200", description = "The updated book object.",
                    content = @Content(mediaType = "application/hal+json",
                            schema = @Schema(type = SchemaType.OBJECT, implementation = BookHal.class))
            )
    })
    @Parameters({
            @Parameter(name = "updatedBook", description = "Update book object.", required = true,
                    content = @Content(schema = @Schema(implementation = Book.class)))
    })
    public Response updateBook(@Context UriInfo uriInfo, @Valid Book updatedBook) {
        LOG.log(Level.INFO, "REST request to update Book : {0}", updatedBook);
        if (updatedBook.getId() == null) {
            throw new RuntimeException("A Book need no id for update.");
        }
        Book book = bookService.edit(updatedBook);
        return Response
                .ok(bookMapper.map(book, uriInfo))
                .build();
    }

}
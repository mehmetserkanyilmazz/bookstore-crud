package com.msy.bookstore.book;

import com.msy.bookstore.entity.Book;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import static javax.transaction.Transactional.TxType.REQUIRED;


@ApplicationScoped
public class BookService {

    private static final Logger LOG = Logger.getLogger(BookService.class.getName());

    @Inject
    private BookRepository bookRepository;

    @Transactional(REQUIRED)
    public void create(Book book) {
        LOG.log(Level.INFO, "SERVICE call to save Book : {0}", book);
        bookRepository.create(book);
    }

    @Transactional(REQUIRED)
    public void remove(Book book) {
        LOG.log(Level.INFO, "BOOK call to delete Book : {0}",book);
        bookRepository.remove(book);
    }

    public Optional<Book>find(Long id){
        LOG.log(Level.INFO,"Service call to find book : {0}",id);
        return Optional.ofNullable(bookRepository.find(id));
    }

    public List<Book> findAll() {
        LOG.log(Level.INFO,"Service call to find all books");
        return bookRepository.findAll();
    }
    @Transactional(REQUIRED)
    public Book edit(Book book) {
        LOG.log(Level.INFO,"Service call to edit book : {0}",book);
        return bookRepository.edit(book);
    }
}

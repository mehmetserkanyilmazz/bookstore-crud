package com.msy.bookstore.book;

import com.msy.bookstore.entity.Book;

import javax.ejb.Stateless;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

// @ApplicationScoped
@Stateless
public class BookRepository {

	@PersistenceContext(unitName = "JPADatasourceExamplePU")
	private EntityManager em;

	public void create(Book book) {
		em.persist(book);
	}

	public Book edit(Book book) {
		em.merge(book);
		return book;
	}

	public void remove(Book book) {
		em.remove(book);
	}

	public Book find(Long id) {
		return em.find(Book.class, id);
	}

	public List<Book> findAll() {
		List<Book> abc = em.createQuery("SELECT p.id, p.name, p.type, p.price, p.transactionTimeZone FROM Book p", Book.class).getResultList();
		return abc;
	}
}

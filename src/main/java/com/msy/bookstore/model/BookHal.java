package com.msy.bookstore.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.openapitools.jackson.dataformat.hal.HALLink;
import io.openapitools.jackson.dataformat.hal.annotation.Resource;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.jetbrains.annotations.Contract;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Date;

@Resource
@Schema(name="BooknHal", description="POJO that represents a book as Hal.")
public class BookHal {

    @Schema(required = true, example = "100")
    private Long id;

    @Schema(required = true, example = "Hanz Mustermann")
    private String name;

    @Schema(required = true, example = "Musterstrasse 12")
    private Long price;

    @Schema(required = true, example = "Musterstrasse 12")
    private String type;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @Schema(required = true, example = "2020-12-20")
    private Timestamp transactionTimeZone;

    private BookHal(Long id, String name, Long price, String type, Timestamp transactionTimeZone) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.price = price;
        this.transactionTimeZone = transactionTimeZone;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getTransactionTimeZone() {
        return transactionTimeZone;
    }

    public void setTransactionTimeZone(Timestamp transactionTimeZone) {
        this.transactionTimeZone = transactionTimeZone;
    }

    @NotNull
    @Contract("_, _, _, _, _, _ -> new")
    public static BookHal of(Long id, String name, String type, Long price, Timestamp transactionTimeZone, HALLink halLink) {
        return new BookHal(id, name, price, type, transactionTimeZone);
    }

}

package com.msy.bookstore.mapper;

import io.openapitools.jackson.dataformat.hal.HALLink;
import com.msy.bookstore.book.BookResource;
import com.msy.bookstore.entity.Book;
import com.msy.bookstore.model.BookHal;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.core.UriInfo;
import java.sql.Timestamp;

@ApplicationScoped
public class BookMapper {

    public BookHal map(Book book, UriInfo uriInfo) {
        return BookHal.of(
                book.getId(),
                book.getName(),
                book.getType(),
                book.getPrice(),
                (Timestamp) book.getTransactionTimeZone(),
                selfLink(book, uriInfo));
    }
    private HALLink selfLink(Book book, UriInfo uriInfo) {
        return new HALLink.Builder(uriInfo.getBaseUriBuilder()
                .path(BookResource.class)
                .path(BookResource.class, "getBook")
                .build(book.getId()))
                .build();
    }

}

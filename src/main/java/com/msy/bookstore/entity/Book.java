package com.msy.bookstore.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity
//@Schema(name = "Book", description = "POJO that represents a book.")
@Table(name = "BOOK")
public class Book {

    @Id
//    @SequenceGenerator(name = "BOOK_ID_SEQ_GENERATOR", sequenceName = "BOOK_ID_SEQ", allocationSize = 1)
//    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator ="BOOK_ID_SEQ_GENERATOR")
    @Column(name = "ID", length = 50)
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "TYPE", length = 50)
    private String type;

    @Column(name = "PRICE")
    private Long price;

    //@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TRANSACTIONTIMEZONE")
    private Timestamp transactionTimeZone;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String address) {
        this.type = address;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Date getTransactionTimeZone() {
        return transactionTimeZone;
    }

    public void setTransactionTimeZone(Timestamp transactionTimeZone) {
        this.transactionTimeZone = transactionTimeZone;
    }

    @Override
    public String toString() {
        return "Book{" + "id=" + id + ", name=" + name + ", type=" + type + "price=" + price + "transactionTimeZone=" + transactionTimeZone + '}';
    }
}
